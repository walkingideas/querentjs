# QuerentJS

Engine for procedural storytelling via WaveFunctionCollapse

## Getting Started

### Installing

```
npm install querentjs
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

```
npm run test
```
