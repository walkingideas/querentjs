import { Querent } from "../index";

test("Trivial test", () => {
  expect(Querent("hello")).toBe("Hello hello");
});
